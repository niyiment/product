
import 'dart:async';
import 'dart:io' as io;
import 'package:Product/model/product.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseProvider {
  static final DatabaseProvider _instance = new DatabaseProvider.internal();
  factory DatabaseProvider() => _instance;
  static Database _db;

  Future<Database> db() async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  DatabaseProvider.internal();

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "product.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    // When creating the db, create the table
    String sqlUser="CREATE TABLE products(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, price REAL NOT NULL)";
    await db.execute(sqlUser);
    _seedProduct(db);
  }

  Future <void> _seedProduct(Database db) async{
    var prod = Product(name: 'Samsung', price: 200.0);
    //String sqlInsert = 'INSERT INTO products VALUES (${prod.name}, ${prod.price})';
    db.rawInsert("INSERT INTO products(`name`, `price`) VALUES ('${prod.name}', ${prod.price})");
  }
  
}