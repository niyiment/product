
import 'dart:async';
import 'package:Product/model/product.dart';
import 'package:sqflite/sqflite.dart';
import 'database_provider.dart';

class ProductRepository{
  DatabaseProvider databaseProvider = new DatabaseProvider();
  ProductDao dao = new ProductDao();

  Future<Product> save(Product product) async{
    Database dbase  = await databaseProvider.db();
    product.id = await dbase.insert(dao.table, dao.toMap(product));

    return product;
  }

  Future<Product> update(Product product) async{
    Database dbase = await databaseProvider.db();
    product.id = await dbase.update(dao.table, 
      dao.toMap(product), where: dao.productId + ' = ?',
      whereArgs:  [product.id]
    );

    return product;
  }

  delete(int id) async {
    final dbase = await databaseProvider.db();
    dbase.delete(dao.table, where: dao.productId + ' = ?', whereArgs: [id]);
  }

  deleteAll() async{
    final dbase = await databaseProvider.db();
    dbase.delete(dao.table);
  }

  Future<List<Product>> all() async{
    Database dbase = await databaseProvider.db();
    List<Map> maps = await dbase.query(dao.table, orderBy: 'name');
    return dao.fromList(maps);
  }

  Future<Product> find(int id) async {
    Database dbase = await databaseProvider.db();
    var response = await dbase.query(dao.table,
      where: dao.productId + '= ?',
      whereArgs: [id]
    );

    return response.isNotEmpty ? dao.fromMap(response.first) : Null;
  }
  

}