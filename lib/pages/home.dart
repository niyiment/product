import 'package:Product/model/product.dart';
import 'package:Product/service/product_repository.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  TextEditingController _name = new TextEditingController();
  TextEditingController _price = new TextEditingController();
  ProductRepository repo = new ProductRepository();
  Future<List<Product>> products;

  @override
  void initState() {
    refreshList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Product List'),
        actions: <Widget>[
          IconButton(
            tooltip: 'New Product',
            icon: Icon(Icons.add),
            onPressed: () {
              productDialog(context, null, false);
            },
          )
        ],
      ),
      body: FutureBuilder(
        future: products,
        builder: (BuildContext context, snapshot) {
          if (snapshot.hasData) {
            return _productListView(snapshot.data);
          }
          if (snapshot.data == null || snapshot.data.length == 0) {
            return Center(child: Text('No record'));
          }

          return CircularProgressIndicator();
        },
      ),
    );
  }

  void _clearInput() {
    _name.text = '';
    _price.text = '';
  }

  void refreshList() {
    setState(() {
      products = repo.all();
    });
  }

  Widget inputField(String labelName, TextEditingController _controller,
      Icon fieldIcon, TextInputType fieldType) {
    var textField = new Padding(
      padding: const EdgeInsets.all(5.0),
      child: new TextFormField(
        controller: _controller,
        keyboardType: fieldType,
        decoration: new InputDecoration(
          icon: fieldIcon,
          labelText: labelName,
          labelStyle:
              TextStyle(color: Colors.purple, fontWeight: FontWeight.w300),
          hintText: labelName,
        ),
        validator: (val) => val.length == 0 ? labelName : null,
        //onSaved: (val) => _controller.text = val,
      ),
    );

    return textField;
  }

  productDialog(BuildContext context, Product prod, bool isEdit) async {
    final formKey = new GlobalKey<FormState>();
    if (prod != null) {
      _name.text = prod.name;
      _price.text = prod.price.toString();
    }

    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text((isEdit) ? 'Edit Data' : 'Add Data',
                style:
                    TextStyle(fontSize: 15.0, color: Colors.deepPurpleAccent)),
            content: Form(
              key: formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  inputField('Name', _name, Icon(Icons.donut_small),
                      TextInputType.text),
                  inputField('Price', _price,
                      Icon(Icons.account_balance_wallet), TextInputType.number),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                padding: EdgeInsets.all(10.0),
                onPressed: () {
                  if (formKey.currentState.validate()) {
                    print('validated');
                    if (_name.text != '' && _price.text != '') {
                      Product product = new Product(
                          name: _name.text.trim(),
                          price: double.tryParse(_price.text.trim()));

                      if (isEdit) {
                        product.id = prod.id;
                        repo.update(product);
                      } else {
                        repo.save(product);
                      }
                      _clearInput();
                      refreshList();
                      Navigator.of(context).pop();
                    }
                  }
                },
                color: Colors.purpleAccent,
                child: Text(
                  (isEdit) ? 'Edit' : 'Add',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              
              FlatButton(
                padding: EdgeInsets.all(10.0),
                onPressed: () {
                  _clearInput();
                  Navigator.pop(context);
                },
                color: Colors.blueGrey,
                child: Text(
                  'Cancel',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          );
        });
  }

  ListView _productListView(List<Product> product) {
    return ListView.builder(
      itemCount: product.length,
      itemBuilder: (context, index) {
        return Column(
          children: <Widget>[
            Dismissible(
              key: UniqueKey(),
              background: Container(
                color: Colors.red,
              ),
              onDismissed: (direction) {
                repo.delete(product[index].id);
                Scaffold.of(context).showSnackBar(SnackBar(
                    backgroundColor: Colors.purpleAccent,
                    content: Text('Record deleted successfully.')));
              },
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: CircleAvatar(
                      child: Text(product[index].name[0].toUpperCase())
                    ),
                    title: Text(product[index].name.toUpperCase(), style: TextStyle(
                      fontSize: 18.0
                    ),),
                    trailing: Text('\$' +product[index].price.toString()),
                    onTap: () {
                      edit(product[index], context);
                    },
                  ),
                  Divider(color: Colors.deepPurpleAccent)
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  edit(Product product, BuildContext context) {
    productDialog(context, product, true);
  }

}
