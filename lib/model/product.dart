
class Product{
  int id;
  String name;
  num price;
  
  Product({this.id, this.name, this.price});
}

class ProductDao{
  final table = 'products';
  final productId = 'id';
  final _name = 'name';
  final _price = 'price';

  Product fromMap(Map<String, dynamic> data){
    Product prod = new Product();
    prod.id = data[productId];
    prod.name = data[_name];
    prod.price = data[_price];

    return prod;
  }
 
  Map<String, dynamic> toMap(Product prod){
    return {
      productId: prod.id,
      _name: prod.name,
      _price: prod.price
    };
  }
      
  List<Product> fromList(List<Map<String, dynamic>> data) {
    List<Product> prods = List<Product>();
    for (Map map in data) {
      prods.add(fromMap(map));
    }
    return prods;
  }
}
